# Compose Control

A command line control system for running, building & watching the log output from the docker processes contained within a docker compose file.  When working with a large docker compose file, the management of it, with the standard docker compose commands can be cumbersome.  This tool was put together to contain all of the commands and functions needed to run, monitor and restart a complex docker compose file with the ability to point out to a custom build script, if you use one which will build the containers.

## Prerequisites

You will need the following applications and utilities installed on your system to run this utility.

* Docker
* Docker Compose
* Python3
* Pip3
* Python pip3 virtualenv
* Linux/Mac type OS (Tested on Mac OSX and Ubuntu 18.04)
* screen
* kubectl (If you want to view a kube config setup for the deploy of your containers) (OPTIONAL)

## Getting Started

Ensure you have above prerequisites installed on your system.  Download the latest version of the source repository and ensure you are running python3.  

In the repository directory run:

```bash
make virtualenv
source .env/bin/activate
```

This will set up a virtual env with the ccontrol utility installed.

## Description

Compose control expects you to set up a number of configuration items.  It expects you to set a compose file which it will control.  You can also set the services in the compose file which you do not want to log.  Save and load various configurations and set a build script, if you have a secondary script that you use to build your containers.

Compose control is built on top of the GNU Screen utility to allow multi screen logging functionality within a single shell.

### Getting help

Compose control contains a number of sub commands.  You can get a list of all of the available command using the -h flag:

```bash 
ccontrol -h
```

Which will display the following output:

```
usage: ccontrol [-h] [-d] [-q] [-v] {configure,logs,start,interactive} ...

Control for Docker Compose

optional arguments:
  -h, --help            show this help message and exit
  -d, --debug           full application debug mode
  -q, --quiet           suppress all console output
  -v, --version         show program's version number and exit

sub-commands:
  {configure,logs,start,interactive}
    configure           configure the compose system to use
    logs                show the log files for the docker compose
    start               start the docker compose file
    interactive         interactive control of the docker compose file

Usage: ccontrol command --foo bar
```

You can get help on each of the various sub-commands by calling the -h flag on the sub-command.

```bash
ccompose control configure -h
```

Which will display the following output:

```
usage: ccontrol configure [-h] [-f COMPOSE_FILE] [-x EXCLUDE_SERVICE]
                          [-i INCLUDE_SERVICE] [-s SAVE_CONFIGURATION]
                          [-r REMOVE_CONFIGURATION] [-l LOAD_CONFIGURATION]
                          [-b BUILD_SCRIPT] [-v] [-d] [-e]

optional arguments:
  -h, --help            show this help message and exit
  -f COMPOSE_FILE, --file COMPOSE_FILE
                        set the docker compose file which will be placed under
                        control
  -x EXCLUDE_SERVICE, --exclude-service EXCLUDE_SERVICE
                        mark a service matching this name as to not be shown
                        in the logs
  -i INCLUDE_SERVICE, --include-service INCLUDE_SERVICE
                        remove a service from the logging exclusion
                        functionality
  -s SAVE_CONFIGURATION, --save-configuration SAVE_CONFIGURATION
                        save the current configuration
  -r REMOVE_CONFIGURATION, --remove-configuration REMOVE_CONFIGURATION
                        remove a saved configuration
  -l LOAD_CONFIGURATION, --load-configuration LOAD_CONFIGURATION
                        load a saved configuration
  -b BUILD_SCRIPT, --build-script BUILD_SCRIPT
                        set a build script which can be used to build docker
                        compose files. This expects service name as the
                        parameter for the service to build. If this script
                        contains parameters it can be encapsulated in ''
  -v, --view-configurations
                        view a list of all of the saved configurations
  -d, --disable-kube    disable the view of kubectl components
  -e, --enable-kube     enable the view of kubectl components
```

### Setting the docker compose file to control

Compose control expects and requires that you set a docker compose file that it can execute.  You can set a compose file using the following syntax:

```bash
ccontrol configure -f /path/to/docker-compose.yaml
```

### Starting a docker compose session with the configured file

You can start up the compose control application to execute your specified compose file with the following syntax:

```bash
ccontrol start
```

This will start up the application and will show the running docker processes, some basic information on your docker compose file, an information panel and a list of tabs showing the logs of all of the compose services running.

```
Configuration: Not Saved                           Docker Processes
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                  PORTS
81b00e65630b        postgres            "docker-entrypoint.s…"   2 seconds ago       Up Less than a second   5432/tcp
39427ce69f66        postgres            "docker-entrypoint.s…"   2 seconds ago       Up Less than a second   5432/tcp
7623ae5607f1        postgres            "docker-entrypoint.s…"   2 seconds ago       Up Less than a second   5432/tcp
7c07175acc25        postgres            "docker-entrypoint.s…"   2 seconds ago       Up Less than a second   5432/tcp






                   Docker Compose Configuration              Build script: /tmp/build.sh -s
 +-------+---------+----------------+----------+             Command output will appear in this window.
 | Index | Service | Container Name |  Image   |
 +-------+---------+----------------+----------+
 |   0   |   foo   |      foo       | postgres |
 |   1   |   bar   |      bar       | postgres |
 |   2   |   baz   |      baz       | postgres |
 |   3   |   boo   |      boo       | postgres |
 +-------+---------+----------------+----------+





                  Ctrl x - terminate | Ctrl r - restart service | Ctrl b - rebuild & restart service
[saturn][                               0*Control   1 foo  2 bar  3 boo                               ][11/29 12:07AM]
```

### Navigating the application

You can close the compose control application, which will also terminate all of the docker compose services by using the hotkey:

* Ctrl + x

You can restart a service, which will also restart the logging, if logging is enabled for that service by using the hotkey:

* Ctrl + r

When you choose to restart a service you are asked for an index of the service to restart.  Enter the Index for the service you wish to restart from the Index shown in the Docker Compose Configuration Panel, not from the numbered list of tabs.

You can rebuild a service, if you have set a build script configured by using the hotkey:

* Ctrl + b

When you choose to rebuild a service you will be asked to input a service index.  Again select the index from the Docker Compose Configuration Panel and not from the numbered list of the tabs.  If this service is logged, then the logging tab for this service will also be restarted.

#### Moving tabs

You can navigate the tabs shown from the main console to the individual logs with the following key commands:

* F1  - Move tab left
* F2  - Move tab right

You also move to a numbered tab using the standard screen command:

* Ctrl + a Number

#### Scrolling in a log

To navigate within a log screen, you will need to place the screen into copy mode.  This is standard GNU Screen functionality.  By using the following hotkey you will be able to scroll up the log:

* Ctrl + a Esc

To exit copy mode press the following key:

* Esc

#### More information on GNU Screen

<https://www.gnu.org/software/screen/>


### Excluding a service from logging

You can see in the above example, that though there are four services in the compose file, only three of these are showing logs (as can be seen in the tabbed index at the bottom of the screen).  If you want to exclude a service from the logs you can use the following syntax:

```bash
ccontrol configure -x baz
```

Which will exclude the baz service from logging.

### Including a service in logging

If you wish to re-include a service in the logging list you can run the following command:

```bash
ccontrol configure -i baz
```

This will set the baz service to display its logs when you restart the compose control application/

### Saving and Loading

You can save the current configuration by executing the command:

```bash
ccontrol configure -s save_name
```

If you want to reload a saved configuration you can do so with the following command:

```bash
ccontrol configure -l save_name
```

You can view a list of  all of the saved configurations with the following command:

```bash
ccontrol configure -v
```

### Setting a build file

You can set a file which is used to build the services used in the docker compose file.  This will allow you to rebuild and restart the service from within the application.   This file expects the last argument of the build script to be the name of the service that you wish to rebuild.  How you create your build script is up to you but if you set a build script the command WILL expect the service name to be the last argument in the call.  You can set the build script with the following command:

```bash
ccontrol configure -b '/tmp/build.sh -s'
```

When a build is called, the above script will be called with the service name appended to the end of the script.  The script will be executed in a new screen session and will run from within the directory that the script is located in.

### Storage

All configuration files for the application are held in a hidden folder in your home directory located at:

```
~/.ccontrol
```

## Built With

Python 3 & cement framework.

## Contributing

Suggestions and merge requests are welcome.  Feel free to submit a merge.

## Versioning

Released at tag 1.0.  This application is by no means perfect but is suitable for my needs.    

## License

This project is licensed under the GPL v3.0 License

## Acknowledgments

The Cement Framework and the awesome Prompt Toolkit.

* <https://builtoncement.com/>
* <https://github.com/prompt-toolkit/python-prompt-toolkit>