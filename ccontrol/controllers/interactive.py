from __future__ import unicode_literals, print_function
from prompt_toolkit import print_formatted_text, HTML
from prompt_toolkit.styles import Style
from prompt_toolkit import prompt
from prompt_toolkit.application import run_in_terminal
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit import Application
from prompt_toolkit.document import Document
from prompt_toolkit.buffer import Buffer
from prompt_toolkit.filters import Condition
from prompt_toolkit.application import get_app
from prompt_toolkit.layout.containers import HSplit, VSplit, Window, ConditionalContainer, WindowAlign
from prompt_toolkit.layout.controls import BufferControl, FormattedTextControl
from prompt_toolkit.widgets import VerticalLine, Box, Frame, RadioList, Button, TextArea, SystemToolbar
from prompt_toolkit.layout.layout import Layout
import os, yaml, json
from cement import Controller, ex
from prompt_toolkit import prompt
from cement.utils import fs
from prettytable import PrettyTable
import subprocess
import tempfile
import stat

class Interactive(Controller):

    CONF_FILE = fs.abspath('~/.ccontrol/config.yml')

    class Meta:
        label = 'control'
        stacked_type = 'embedded'
        stacked_on = 'base'

    @ex(
        help='interactive control of the docker compose file',

        # sub-command level arguments. ex: 'cloudplatform configure --foo bar'
        arguments=[],
    )

    def interactive(self):
        """Interactive control of the docker-compose file"""
        # run the docker-compose file
        compose_file = self.app.config.get('ccontrol_config', 'compose_file')
        # used to determine if we need to reopen a screen
        config_file = yaml.load(open(self.CONF_FILE))

        style = Style.from_dict({
            'status': 'reverse'
        })

        @Condition
        def show_input_bar():
            return self.display_input

        @Condition
        def show_build_bar():
            return self.build_input
        
        @Condition
        def show_status_bar():
            if self.display_input  is False and self.build_input is False:
                return True
            else:
                return False

        @Condition
        def show_k8_window():
            show_k8s = False
            if 'kube_enabled' in config_file['ccontrol_config']:
                if config_file['ccontrol_config']['kube_enabled'] is True:
                    show_k8s = True

            return show_k8s

        bindings = KeyBindings()
        @bindings.add('c-x')
        def _(event):
            " Exit when `c-x` is pressed. "
            def print_exit():
                print("Please wait... stopping docker-compose services.")
                subprocess.call(["docker-compose", "-f", compose_file, "down"])
            run_in_terminal(print_exit)
            event.app.exit()

        @bindings.add('c-r')
        def _(event):
            " Restart a service when `c-r` is pressed. "
            if self.display_input is True:
                self.display_input = False
            else:
                self.display_input = True 
                get_app().layout.focus(status_input_field)

        @bindings.add('c-b')
        def _(event):
            " Rebuild & restart a service when `c-b` is pressed. "
            if self.build_input is True:
                self.build_input = False
            else:
                self.build_input = True 
                get_app().layout.focus(build_input_field)

        def restart_service(buff):
            self.display_input = False
            # Attempt to restart the service
            # Need to add a threaded refresh of the UI to show that the service is restarting
            output_text = ''
            if buff.text in self.compose_service_dict.keys():
                FNULL = open(os.devnull, 'w')
                subprocess.call(['docker-compose', '-f', compose_file, 'restart', self.compose_service_dict[buff.text]['service']], stdout=FNULL, stderr=subprocess.STDOUT)
                FNULL.close()
                output_text += "Restarted service '{0}'".format(self.compose_service_dict[buff.text]['service'])
                # check if we have a service we should restart in screen
                # services are excluded by service name check if its contained in the config
                if self.compose_service_dict[buff.text]['service'] not in config_file['ccontrol_config']['excluded_services']:
                   output_text += "\nRestarted screen: {0}".format(self.compose_service_dict[buff.text]['service'])
                   subprocess.call(['screen', '-t', self.compose_service_dict[buff.text]['service'], 'docker', 'logs', '--follow', self.compose_service_dict[buff.text]['service']]) 
                   # I need someway to refocus the previously selected window 
                   subprocess.call(['screen', '-X', 'select', '0']) 

            else:
                output_text = output_text + "\n'{0}' does not seem to be a service index, please select a valid index".format(buff.text)

            self.output_field.buffer.document = Document(
                text=output_text, cursor_position=len(output_text))

        def rebuild_service(buff):
            self.build_input = False
            # Attempt to restart the service
            # Need to add a threaded refresh of the UI to show that the service is restarting
            if 'build_script' in config_file['ccontrol_config']: 
                output_text = 'Attempting to rebuild service using command :{0}\n'.format(config_file['ccontrol_config']['build_script'])

                # check that the service actually exists in the docker compose file
                if buff.text in self.compose_service_dict: 
                    # build a tempfile which we will pass to the screen command for the rebuild
                    # temp output what will be in the build script
                    command = config_file['ccontrol_config']['build_script'].split()
                    script_file = fs.abspath(command[0]) 
                    # check that the script file actually exists
                    if os.path.exists(script_file):
                        output_text += "Script file exists\n"

                        # create a temp file which will store the reboot script in bash format
                        temp = tempfile.NamedTemporaryFile(mode='w+t', delete=False)
                        # write the bash script which will restart the service 
                        temp.writelines("#!/bin/bash\n")
                        temp.writelines("cd {0}\n".format(os.path.dirname(command[0])))
                        temp.writelines("bash {0} {1}\n".format(config_file['ccontrol_config']['build_script'], self.compose_service_dict[buff.text]['service']))
                        temp.writelines("docker-compose -f {0} restart {1}\n".format(compose_file, self.compose_service_dict[buff.text]['service']))

                        # restart the screen if needed
                        if self.compose_service_dict[buff.text]['service'] not in config_file['ccontrol_config']['excluded_services']:
                            output_text += "\nRestarted screen: {0}\n".format(self.compose_service_dict[buff.text]['service'])
                            temp.writelines("screen -t {0} docker logs --follow {1}\n".format(self.compose_service_dict[buff.text]['service'], self.compose_service_dict[buff.text]['service']))

                        # make the file delete itself when the screen command finishes
                        temp.writelines('rm -- "$0"')  
                        # OSX and windoze seem to require the file to be closed to allow it readable by other processes
                        script_filename = temp.name
                        temp.close()

                        # Notify the user
                        output_text += "Executing build script {0} on service {1}\n".format(config_file['ccontrol_config']['build_script'], self.compose_service_dict[buff.text]['service'])
                        
                        # make the file executable, run it and delete the file
                        st = os.stat(script_filename)
                        os.chmod(script_filename, st.st_mode | stat.S_IEXEC)
                        subprocess.call(['screen', '-t', 'BUILDING', format(script_filename)])

                    else:
                        output_text += "SCRIPT FILE DOES NOT EXIST\n\n"
                        output_text += "Cannot restart rebuild service if build script can not be located!\n"
                else:
                    output_text += "SERVICE {0} DOES NOT EXIST\n\n".format(buff.text)
                    output_text += "Cannot restart rebuild service if service does not exist!\n"

            else:
               output_text = 'can not restart services, build script not set' 

            self.output_field.buffer.document = Document(
                text=output_text, cursor_position=len(output_text))


        status_input_field = TextArea(text=u'', multiline=False, focusable=True, style='class:status', accept_handler=restart_service)
        build_input_field  = TextArea(text=u'', multiline=False, focusable=True, style='class:status', accept_handler=rebuild_service)

        root_container = HSplit([
            HSplit([
                VSplit([
                    Window(FormattedTextControl(self.show_saved_configuration(config_file)), style='class:status', align=WindowAlign.LEFT),
                    Window(FormattedTextControl("Docker Processes"), style='class:status', align=WindowAlign.CENTER),
                    Window(FormattedTextControl(' ' * len(self.show_saved_configuration_string(config_file))), style='class:status', align=WindowAlign.RIGHT),
                ], height=1),
                Window(FormattedTextControl(self.get_docker_ps_list), align=WindowAlign.LEFT),
                VSplit([
                    Box(HSplit([
                        VSplit([
                            Window(FormattedTextControl(""), style='class:status', width=6, align=WindowAlign.LEFT),
                            Window(FormattedTextControl("Docker Compose Configuration"), style='class:status', height=1, align=WindowAlign.CENTER),
                        ], height=1),
                        Window(content=FormattedTextControl(text=self.get_compose_services())),
                    ]),padding=1),
                    Box(HSplit([
                        VSplit([
                            Window(FormattedTextControl(self.get_build_script_setting(config_file)), style='class:status', align=WindowAlign.LEFT),
                        ], height=1),
                        self.output_field
                    ]), padding=1),
                    ConditionalContainer(
                        Box(HSplit([
                            VSplit([
                                Window(FormattedTextControl("Ctrl 3"), style='class:status', width=6, align=WindowAlign.LEFT),
                                Window(FormattedTextControl("Kubernetes Pods"), style='class:status', height=1, align=WindowAlign.CENTER),
                            ], height=1),
                            Window(content=FormattedTextControl(text=self.k8s_get_pods())),
                        ]), padding=1),
                        show_k8_window),
                ])
            ]),
            # show the command status bar
            ConditionalContainer(
                VSplit([
                    Window(FormattedTextControl(self.select_index), style='class:status', height=1, width=15, align=WindowAlign.LEFT), 
                    status_input_field
                ]), 
                show_input_bar),
            ConditionalContainer(
                VSplit([
                    Window(FormattedTextControl(self.select_index), style='class:status', height=1, width=15, align=WindowAlign.LEFT), 
                    build_input_field
                ]), 
                show_build_bar),
            ConditionalContainer(Window(FormattedTextControl(self.bottom_toolbar(config_file)), style='class:status', height=1, align=WindowAlign.CENTER), show_status_bar)
        ])

        layout = Layout(root_container)

        guiapp = Application(layout=layout, full_screen=True, key_bindings=bindings, style=style)
        guiapp.run()

    '''
    This is used to store the output in the logging list area.. name to change
    '''    
    output_field = TextArea(text='Command output will appear in this window.') 

    '''
    This flag holds whether or not we should show the input field
    '''
    display_input=False

    '''
    This flag holds whether or not we should show the build input field
    '''
    build_input=False

    '''
    This holds a list of all the services and the index for that service in the docker compose list
    '''
    compose_service_dict = {}

    def get_build_script_setting(self, conf_dict):
       if 'build_script' in conf_dict['ccontrol_config']:
            return HTML('<b>Build script</b>: {0}'.format(conf_dict['ccontrol_config']['build_script']))
       else:
            return HTML('<b>Build script</b>: NOT SET') 

    def bottom_toolbar(self, config_file):
        if 'build_script' in config_file['ccontrol_config']:
            return HTML('<b>Ctrl x</b> - terminate | <b>Ctrl r</b> - restart service | <b>Ctrl b</b> - rebuild &amp; restart service')
        else:
            return HTML('<b>Ctrl x</b> - terminate | <b>Ctrl r</b> - restart service') 

    def show_saved_configuration(self, conf_dict):
        if 'save' in conf_dict['ccontrol_config']:
            return HTML('<b>Configuration</b>: {0}'.format(conf_dict['ccontrol_config']['save']))
        else:
            return HTML('<b>Configuration</b>: Not Saved') 

    def show_saved_configuration_string(self, conf_dict):
       if 'save' in conf_dict['ccontrol_config']:
            return 'Configuration: {0}'.format(conf_dict['ccontrol_config']['save'])
       else:
            return 'Configuration: Not Saved' 

    def select_index(self):
        return HTML('<b>Select Index</b>:')

    def get_docker_ps_list(self):
        return subprocess.check_output(["docker", "ps"]).decode('utf-8', 'ignore')

    def get_compose_services(self):
        self.compose_service_dict = {}
        compose_dict = yaml.load(open(fs.abspath(self.app.config.get('ccontrol_config', 'compose_file'))))
        count = 0
        t = PrettyTable(['Index', 'Service', 'Container Name', 'Image'])
        if compose_dict['services'] is not None:
            for service in compose_dict['services']:
                container = ""
                if 'container_name' in compose_dict['services'][service] and compose_dict['services'][service]['container_name'] is not None:
                    container = compose_dict['services'][service]['container_name']
                t.add_row([count, service, container, compose_dict['services'][service]['image']])
                # set the key as a string to make it easier to search
                container_name = ""
                if 'conatiner_name' in compose_dict['services'][service]:
                    container_name = compose_dict['services'][service]['container_name']
                self.compose_service_dict[str(count)] = {'service' : service, 'container' : container_name}
                count = count+1
        return t.get_string

    def k8s_get_pods(self):
        returnString = ""
        try:
            returnString = subprocess.check_output(["kubectl", "get","pods"]).decode('utf-8', 'ignore')
        except :
            returnString = "No K8s Pods detected\n\nCheck your config\n"
        return returnString

    



