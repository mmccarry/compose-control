import os, yaml
from subprocess import call
from cement import Controller, ex
from cement.utils import fs
from .configure import Configure

class Logs(Controller):
    class Meta:
        label = 'logs'
        stacked_type = 'embedded'
        stacked_on = 'base'

    @ex(
        help='show the log files for the docker compose',
        # sub-command level arguments. ex: 'cloudplatform configure --foo bar'
        arguments=[],
    )

    def logs(self, show_stop_compose = False):
        """Pull the setting from the config for docker compose and workout
        what the services are"""
        compose_file = self.app.config.get('ccontrol_config', 'compose_file')

        # check that the compose file exists.
        compose_file_path = fs.abspath(compose_file)
        if os.path.isfile(compose_file_path) is False:
            # File does not exist
            self.app.render({}, 'configuration_notset.jinja2')
        else:
            # get a list of all of the services in the docker-compose files
            compose_dict = yaml.load(open(compose_file_path))
            # ensure that we have a list of services in the dict
            if compose_dict['services'] is not None:
                # build a list of the services in the compose and exclude any which
                # have been marked as exclude in the configuration
                compose_services = []
                conf_dict = Configure().loadConfigFile()
                if 'excluded_services' in conf_dict['ccontrol_config']:
                  for compose_service in compose_dict['services']:
                      if compose_service not in conf_dict['ccontrol_config']['excluded_services']:
                          compose_services.append(compose_service)
                else:
                  compose_services = compose_dict['services']
                data = {
                  'stop_compose' : show_stop_compose,
                  'compose_file' : compose_file,
                  'compose_path' : os.path.dirname(compose_file_path),
                  'services' : compose_services,
                }
                # write the data out to a screenrc in the users config directory
                screen_file = fs.abspath('~/.ccontrol/screenrc')
                f= open(screen_file, 'w+')
                self.app.render(data, 'screen.jinja2', f)
                f.close()
                # execute the screen command on the screenrc file
                call(['screen', '-c', screen_file])
            else:
                # there were no services, exit
                self.app.render({'compose_file' : compose_file}, 'services_notset.jinja2')
