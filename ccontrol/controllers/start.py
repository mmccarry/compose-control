import os, yaml
from subprocess import call
from cement import Controller, ex
from cement.utils import fs
from .logs import Logs

class Start(Controller):
    class Meta:
        label = 'start'
        stacked_type = 'embedded'
        stacked_on = 'base'

    @ex(
        help='start the docker compose file',

        # sub-command level arguments. ex: 'cloudplatform configure --foo bar'
        arguments=[],
    )

    def start(self):
        """Start the specified docker-compose file"""
        print("running start")
        # run the docker-compose file
        compose_file = self.app.config.get('ccontrol_config', 'compose_file')

        # check that the compose file exists.
        compose_file_path = fs.abspath(compose_file)
        if os.path.isfile(compose_file_path) is False:
            # File does not exist
            self.app.render({}, 'configuration_notset.jinja2')
        else:
            if self.app.config.get('ccontrol', 'start_detached') == True:
                call(['docker-compose', '-f', compose_file, 'up', '-d'])
                if self.app.config.get('ccontrol', 'when_detached_show_logs') == True:
                    Logs.logs(self, True)
                else:
                    print("Docker-Compose Up ...")
                    input("Press Enter to terminate docker-compose...")
                    call(['docker-compose', '-f', compose_file, 'down'])
            else:
                call(['docker-compose', '-f', compose_file, 'up'])
